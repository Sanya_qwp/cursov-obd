import Vue       from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import Events from '../components/Events'
import Addevent from '../components/Addevent'

import Workers from '../components/Workers'
import Addworker from '../components/Addworker'

import Shops from '../components/Shops'
import Addshop from '../components/Addshop'

import Products from '../components/Products'
import Addproduct from '../components/Addproduct'

import Other from '../components/Other'
import Addcity from '../components/Addcity'
import Addbrand from '../components/Addbrand'
import Addtype from '../components/Addtype'

import Auth from '../components/Auth'

import Pagenotfound from '../components/Pagenotfound'

export const router = new VueRouter({
  base: '/',
  mode: 'history',
  routes:[
      // События
      {
        path: '/events',
        name: 'Мастер-классы',
        component: Events
      },
      {
        path: '/addevent',
        name: 'Добавить событие',
        component: Addevent
      },

      // Рабочие
      {
        path: '/workers',
        name: 'Сотрудники',
        component: Workers
      },
      {
        path: '/addworker',
        name: 'Добавить работника',
        component: Addworker
      },

      // Магазины
      {
        path: '/shops',
        name: 'Магазины',
        component: Shops
      },
      {
        path: '/addshop',
        name: 'Добавить магазин',
        component: Addshop
      },

      // Товары
      {
        path: '/products',
        name: 'Товары',
        component: Products
      },
      {
        path: '/addproduct',
        name: 'Добавить позицию',
        component: Addproduct
      },

      // Кучка, созданная для добавления инфы
      {
        path: '/other',
        name: 'Города' ,
        component: Other
      },
      {
        path: '/addcity',
        name: 'Добавить город',
        component: Addcity
      },
      {
        path: '/addbrand',
        name: 'Добавить город',
        component: Addbrand
      },
      {
        path: '/addtype',
        name: 'Добавить материал',
        component: Addtype
      },

      // Вход
      {
        path: '/auth',
        name: 'Вход',
        component: Auth
      },

      //404
      {
        path: '*',
        name: 'Страница не найдена',
        component: Pagenotfound
      }
  ]
})
