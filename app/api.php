<?
  require_once('db.php'); //Подключение конфига db.php
  session_start();
  $restrictedAllowedURL = "/auth";
  $startPageURL = "/products";
  $requestUri = $_SERVER['REQUEST_URI'];
  $request_body = file_get_contents('php://input');
  $data = json_decode($request_body);


  // Если пользователь вне сессии
  if(!isset($_SESSION['session_username']) && ($requestUri != $restrictedAllowedURL)){
    header("Location: ".$restrictedAllowedURL);
  }
  else if(isset($_SESSION['session_username']) && ($requestUri == $restrictedAllowedURL)){
    header("Location: ".$startPageURL);
  }

  // Роутинг. 80(может быть больше) строк удовольствия

  // Товары
  if ($requestUri === '/api/products') {
    getProducts(); die();
    authCheck();
  }
  else if ($requestUri === '/api/add-product') {
    addProduct($data); die();
  }
  else if ($requestUri === '/api/remove-product') {
    removeProduct($data); die();
  }

  // Магазины
  else if ($requestUri === '/api/shops') {
    getShops(); die();
  }
  else if ($requestUri === '/api/add-shops') {
    addShop($data); die();
  }
  else if ($requestUri === '/api/remove-shop') {
    removeShop($data); die();
  }

  // Рабочие
  else if ($requestUri === '/api/workers') {
    getWorkers(); die();
  }
  else if ($requestUri === '/api/add-worker') {
    addWorker($data); die();
  }
  else if ($requestUri === '/api/remove-worker') {
    removeWorker($data); die();
  }

  // Бренды
  else if ($requestUri === '/api/brands') {
    getBrands(); die();
  }
  else if ($requestUri === '/api/add-brand') {
    addBrand($data); die();
  }
  else if ($requestUri === '/api/remove-brand') {
    removeBrand($data); die();
  }

  // Типы
  else if ($requestUri === '/api/types') {
    getTypes(); die();
  }
  else if ($requestUri === '/api/add-type') {
    addType($data); die();
  }
  else if ($requestUri === '/api/remove-type') {
    removeType($data); die();
  }

  // Календарь событий
  else if ($requestUri === '/api/events'){
    getEvents(); die();
  }
  else if ($requestUri === '/api/add-event'){
    addEvent($data); die();
  }
  else if ($requestUri === '/api/remove-event'){
    removeEvent($data); die();
  }

  // Города
  else if ($requestUri === '/api/cities'){
    getCities(); die();
  }
  else if ($requestUri === '/api/add-city'){
    addCity($data); die();
  }
  else if ($requestUri === '/api/remove-city'){
    removeCity($data); die();
  }

  // Авторизация
  else if ($requestUri === '/api/auth') {
    auth($data); die();
  }
  else if ($requestUri === '/api/logout') {
    logout(); die();
  }

  // Методы

  // Методы авторизации
  function auth($data){
    global $link;
    $query = "SELECT * FROM users WHERE user='".$data->user."' AND password = '".md5(md5($data->password))."'";
    if ($result = mysqli_query($link, $query)){
      while ($row = $result->fetch_assoc()) {
        $_SESSION['session_username'] = $row["user"];
        // header("Location: /");
      }
    }
    mysqli_close($link);
  }
  function logout(){
    session_unset();
    session_destroy();
    header("Location: /auth");
  }


  // Методы для продуктов
  function getProducts(){
    global $link;
    $query = "SELECT * FROM products ORDER BY id DESC";
    $result = mysqli_query($link, $query) or die("Не удалось подключиться к БД" . mysqli_error($link));
    $array = [];

    if ($result) {
      $rows = mysqli_num_rows($result);

      for ($i = 0 ; $i < $rows ; ++$i){
        $row = mysqli_fetch_row($result);

        $object = (object)[];
        $object->id = (int) $row[0];
        $object->brand_id = (int) $row[1];
        $object->shop_id = (int) $row[2];
        $object->type_id = (int) $row[3];
        $object->name = $row[4];
        $object->price = $row[5];
        $object->count = (int) $row[6];

        $object->brand = getBrandById($object->brand_id)->name;
        $object->type = getTypeById($object->type_id)->name;
        array_push($array, $object);
      }
      mysqli_free_result($result);
    }
    mysqli_close($link);
    echo json_encode($array);
  }
  function addProduct($data) {
    global $link;
    // product: {
    //   name: 'Карандаш',
    //   brand: 2,
    //   type: 1,
    //   price: 25,
    //   count: 1000,
    //   area: 'Кемерово',
    //   shopId: 1
    // }
    $query = "INSERT INTO products (brand_id, shop_id, type_id, name, price, count) VALUES (".$data->brandId.", ".$data->shopId.", ".$data->typeId.", '".$data->name."', ".$data->price.", ".$data->count.")";
    $result = mysqli_query($link, $query) or die("Не удалось подключиться к БД" . mysqli_error($link));
  }
  function removeProduct($data){
    global $link;
    $query = "DELETE FROM products WHERE ".$data->id." = products.id";
    $result = mysqli_query($link, $query) or die("Не удалось подключиться к БД" . mysqli_error($link));
  }

  // Методы для магазинов
  function getShops(){
    global $link;
    $query = "SELECT * FROM shops ORDER BY id DESC";
    $result = mysqli_query($link, $query) or die("Не удалось подключиться к БД" . mysqli_error($link));
    $array = [];

    if ($result) {
      $rows = mysqli_num_rows($result);

      for ($i = 0 ; $i < $rows ; ++$i){
        $row = mysqli_fetch_row($result);

        $object = (object)[];
        $object->id = (int) $row[0];
        $object->city_id = (int) $row[1];
        $object->name = $row[2];
        $object->address = $row[3];
        $object->phone = $row[4];

        array_push($array, $object);
      }
      mysqli_free_result($result);
    }
    mysqli_close($link);
    echo json_encode($array);
  }
  function addShop($data){
    global $link;
    $query = "INSERT INTO shops (city_id, name, address, phone) VALUES (".$data->cityId.", '".$data->name."', '".$data->address."', ".$data->phone.")";
    $result = mysqli_query($link, $query) or die("Не удалось подключиться к БД" . mysqli_error($link));
  }
  function removeShop($data){
    global $link;
    $query = "DELETE FROM shops WHERE ".$data->id." = shops.id";
    $result = mysqli_query($link, $query) or die("Не удалось подключиться к БД" . mysqli_error($link));
  }

  // Методы для работников
  function getWorkers(){
    global $link;
    $query = "SELECT * FROM workers";
    $result = mysqli_query($link, $query) or die("Не удалось подключиться к БД" . mysqli_error($link));
    $array = [];

    if ($result) {
      $rows = mysqli_num_rows($result);
      for ($i = 0 ; $i < $rows ; ++$i){
        $row = mysqli_fetch_row($result);
        $object = (object)[];
        $object->id = (int) $row[0];
        $object->shop_id = $row[1];
        $object->user_id = $row[2];
        $object->firstname = $row[3];
        $object->secondname = $row[4];
        $object->midname = $row[5];
        $object->phone = $row[6];
        $object->shopname = getShopById($object->shop_id)->name;
        array_push($array, $object);
      }
      mysqli_free_result($result);
    }
    mysqli_close($link);
    echo json_encode($array);
  }
  function addWorker($data){
    global $link;
    $query = "INSERT INTO workers (firstname, secondname, midname, phone, shop_id) VALUES ('".$data->firstname."', '".$data->secondname."', '".$data->midname."', ".$data->phone.", ".$data->shopId.")";
    $result = mysqli_query($link, $query) or die("Не удалось подключиться к БД" . mysqli_error($link));
  }
  function removeWorker($data){
    global $link;
    $query = "DELETE FROM workers WHERE ".$data->id." = workers.id";
    $result = mysqli_query($link, $query) or die("Не удалось подключиться к БД" . mysqli_error($link));
  }

  // Методы для брендов
  function getBrands(){
    global $link;
    $query = "SELECT * FROM brands ORDER BY id DESC";
    $result = mysqli_query($link, $query) or die("Не удалось подключиться к БД" . mysqli_error($link));
    $array = [];

    if ($result) {
      $rows = mysqli_num_rows($result);
      for ($i = 0 ; $i < $rows ; ++$i){
        $row = mysqli_fetch_row($result);
        $object = (object)[];
        $object->id = (int) $row[0];
        $object->name = $row[1];
        $object->description = $row[2];

        array_push($array, $object);
      }
      mysqli_free_result($result);
    }
    mysqli_close($link);
    echo json_encode($array);
  }
  function addBrand($data){
    global $link;
    $query = "INSERT INTO brands (name, description) VALUES ('".$data->name."', '".$data->desc."')";
    $result = mysqli_query($link, $query) or die("Не удалось подключиться к БД" . mysqli_error($link));
    die(var_dump($result));
  }
  function removeBrand($data){
    global $link;
    $query = "DELETE FROM brands WHERE ".$data." = brands.id";
    $result = mysqli_query($link, $query) or die("Не удалось подключиться к БД" . mysqli_error($link));
  }

  //Методы для типов
  function getTypes(){
    global $link;
    $query = "SELECT * FROM types ORDER BY id DESC";
    $result = mysqli_query($link, $query) or die("Не удалось подключиться к БД" . mysqli_error($link));
    $array = [];

    if ($result) {
      $rows = mysqli_num_rows($result);
      for ($i = 0 ; $i < $rows ; ++$i){
        $row = mysqli_fetch_row($result);
        $object = (object)[];
        $object->id = (int) $row[0];
        $object->name = $row[1];
        $object->description = $row[2];
        array_push($array, $object);
      }
      mysqli_free_result($result);
    }
    mysqli_close($link);
    echo json_encode($array);
  }
  function addType($data){
    global $link;
    $query = "INSERT INTO types (name, description) VALUES ('".$data->name."', '".$data->desc."')";
    $result = mysqli_query($link, $query) or die("Не удалось подключиться к БД" . mysqli_error($link));
  }
  function removeType($data){
    global $link;
    $query = "DELETE FROM types WHERE ".$data." = types.id";
    $result = mysqli_query($link, $query) or die("Не удалось подключиться к БД" . mysqli_error($link));
  }

  // Методы для событий
  function getEvents(){
    global $link;
    $query = "SELECT * FROM events ORDER BY id DESC";
    $result = mysqli_query($link, $query) or die("Не удалось подключиться к БД" . mysqli_error($link));
    $array = [];

    if ($result) {
      $rows = mysqli_num_rows($result);

      for ($i = 0 ; $i < $rows ; ++$i){
        $row = mysqli_fetch_row($result);

        $object = (object)[];
        $object->id = (int) $row[0];
        $object->worker_id = $row[1];
        $object->shop_id = $row[2];
        $object->name = $row[3];
        $object->description = $row[4];
        $object->date = $row[5];
        $object->worker = getWorkerById($object->worker_id);
        $object->workerFirstName = $object->worker->firstname;
        $object->workerSecondName = $object->worker->secondname;
        $object->workerMidName = $object->worker->midname;
        $object->shop = getShopById($object->shop_id)->address;

        array_push($array, $object);
      }
      mysqli_free_result($result);
    }
    mysqli_close($link);
    echo json_encode($array);
  }
  function addEvent($data){
    global $link;
    $query = "INSERT INTO events (worker_id, shop_id, name, description, date) VALUES (".$data->workerId.", ".$data->shopId.", '".$data->name."', '".$data->desc."', '".$data->date."')";
    $result = mysqli_query($link, $query) or die("Не удалось подключиться к БД" . mysqli_error($link));
  }
  function removeEvent($data){
    global $link;
    $query = "DELETE FROM events WHERE ".$data->id." = events.id";
    $result = mysqli_query($link, $query) or die("Не удалось подключиться к БД" . mysqli_error($link));
  }

  //Методы для городов
  function getCities(){
    global $link;
    $query = "SELECT * FROM cities ORDER BY id DESC";
    $result = mysqli_query($link, $query) or die("Не удалось подключиться к БД" . mysqli_error($link));
    $array = [];

    if ($result) {
      $rows = mysqli_num_rows($result);

      for ($i = 0 ; $i < $rows ; ++$i){
        $row = mysqli_fetch_row($result);
        $object = (object)[];
        $object->id = (int) $row[0];
        $object->name = $row[1];
        $object->ismillionaire = $row[2];
        array_push($array, $object);
      }
      mysqli_free_result($result);
    }
    mysqli_close($link);
    echo json_encode($array);
  }
  function addCity($data){
    global $link;
    $query = "INSERT INTO cities (name) VALUES ('".$data->name."')";
    $result = mysqli_query($link, $query) or die("Не удалось подключиться к БД" . mysqli_error($link));
  }
  function removeCity($data){
    global $link;
    $query = "DELETE FROM cities WHERE ".$data." = cities.id";
    $result = mysqli_query($link, $query) or die("Не удалось подключиться к БД" . mysqli_error($link));
  }

  // Воспомогательные методы для получения данных
  // return object
  function getShopById($id){
    global $link;
    $query = "SELECT * FROM shops WHERE (id = '$id')";
    $result = mysqli_query($link, $query) or die("Не удалось подключиться к БД" . mysqli_error($link));
    if ($result){
      $rows = mysqli_num_rows($result);
      $row = mysqli_fetch_row($result);
      $object = (object)[];
      $object->id = (int) $row[0];
      $object->city_id = (int) $row[1];
      $object->name = $row[2];
      $object->address = $row[3];
      $object->phone = $row[4];
      return $object;
    }
    else {
      return('Магазин не найден');
    }
  }
  function getBrandById($id){
    global $link;
    $query = "SELECT * FROM brands WHERE (id = '$id')";
    $result = mysqli_query($link, $query) or die("Не удалось подключиться к БД" . mysqli_error($link));
    if ($result){
      $rows = mysqli_num_rows($result);
      $row = mysqli_fetch_row($result);
      $object = (object)[];
      $object->id = (int) $row[0];
      $object->name = $row[1];
      return $object;
    }
    else {
      return('Бренд не найден');
    }
  }
  function getTypeById($id){
    global $link;
    $query = "SELECT * FROM types WHERE (id = '$id')";
    $result = mysqli_query($link, $query) or die("Не удалось подключиться к БД" . mysqli_error($link));
    if ($result){
      $rows = mysqli_num_rows($result);
      $row = mysqli_fetch_row($result);
      $object = (object)[];
      $object->id = (int) $row[0];
      $object->name = $row[1];
      return $object;
    }
    else {
      return('Тип не найден');
    }
  }
  function getWorkerById($id){
    global $link;
    $query = "SELECT * FROM workers WHERE (id = '$id')";
    $result = mysqli_query($link, $query) or die("Не удалось подключиться к БД" . mysqli_error($link));
    if ($result){
      $rows = mysqli_num_rows($result);
      $row = mysqli_fetch_row($result);
      $object = (object)[];
      $object->id = (int) $row[0];
      $object->firstname = $row[2];
      $object->secondname = $row[3];
      $object->midname = $row[4];
      return $object;
    }
    else {
      return('Тип не найден');
    }
  }
?>
