var admin = require('firebase-admin');
import * as admin from 'firebase-admin';

var serviceAccount = require("cursov-obd-firebase-adminsdk-3gt10-bb4080462f.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://cursov-obd.firebaseio.com"
});

