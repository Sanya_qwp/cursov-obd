<?
  error_reporting(-1);
  header('Content-Type: text/html; charset=utf-8');
  require_once './app/api.php';
  $whitelist = array(
    '127.0.0.1',
    '::1'
  );

  $currentURL = $_SERVER['REQUEST_URI'];

  //Проверка, является ли сервер локальным
  if(in_array($_SERVER['REMOTE_ADDR'], $whitelist)) {
    $homeDir = 'http://localhost:8080/';
  }
  else {
    $homeDir = '/';
  }


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>course-work--database</title>
    <link rel="stylesheet" href="<? echo $homeDir; ?>dist/normalize.css">
    <link rel="stylesheet" href="<? echo $homeDir; ?>dist/bootstrap.min.css">
</head>
<body>
  <? if (($currentURL != "/auth") && ($currentURL != "http://my-project-dev.ru/auth")): ?>
    <div id="header"></div>
  <? endif; ?>
  <div class="container-fluid" id="app"></div>
  <script src="<? echo $homeDir; ?>dist/build.js"></script>
  <script src="<? echo $homeDir; ?>dist/jquery.min.js"></script>
  <script src="<? echo $homeDir; ?>dist/bootstrap.min.js"></script>
</body>
</html>
