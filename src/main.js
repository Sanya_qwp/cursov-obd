import Vue       from 'vue'
import axios     from 'axios'
import VueAxios  from 'vue-axios'
import Vuex      from 'vuex';
import VueRouter from 'vue-router'
import Bootstrap from 'bootstrap'

import Header from './components/Header'
import App from './App'

import { router } from './router/router'
import { store } from './store/store'

Vue.use(Vuex)
Vue.use(VueRouter)
Vue.use(VueAxios, axios)

new Vue({
  el: '#header',
  router,
  render: h => h(Header)
})
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
